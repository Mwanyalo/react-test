import { GitHubActivityData, GitHubUserSearchData } from "./types";

import { Octokit } from "@octokit/rest";

const octokit = new Octokit();

export default class GitHubService {
  static readonly GIT_API_URL = "https://api.github.com/";

  public static async getUserActivity(
    username: string
  ): Promise<GitHubActivityData> {
    let userRepos: any;
    try {
      userRepos = await octokit.request<any>(`GET /users/${username}/repos`);
      const {data } = userRepos;
      return data;
    } catch (error: any) {
      console.log(error.message);
      throw new Error(`User ${error.message}`);
    }
  }
  public static async getUserProfile( username: string): Promise<any> {
    let userProfile: any;
    try {
      userProfile = await octokit.request<any>(`GET /users/${username}`);
      const {data } = userProfile;
      return data;
    } catch (error: any) {
      console.log(error.message);
      throw new Error(error.message);
    }
  }
  public static async searchUsers(
    username: string,
    page?: number
  ): Promise<GitHubUserSearchData> {
    // let usersList : GitHubUserSearchData = {} as GitHubUserSearchData;
    let usersList: any;
    try {
      usersList = await octokit.request<any>("GET /search/users", {
        q: username,
        sort: "followers",
        page: page,
        order: "desc",
      });
      const {data } = usersList;
      return data;
    } catch (error: any) {
      console.log(error.message);
      throw new Error(error.message);
    }
  }
}
