import { useEffect } from "react";
import { useParams } from "react-router-dom";
import ActivityList from "../components/ActivityList/ActivityList";
import { Preloader } from "../components/Preloader/Preloader";
import UserProfile from "../components/UserProfile/UserProfile";

const UserActivityView = (props: any) => {
  const { username } = useParams<{ username: string }>();
  const { getUserRepos, getUser } = props;

  useEffect(() => {
    console.log(username);
    getUserRepos(username);
    getUser(username);
  }, [username]);

  return (
    <div className="container">
      <div className="col-left">
        <UserProfile {...props.user} />
        {props.isLoading && <Preloader />}
      </div>

      <div className="col-right">
      {props.isLoading && <Preloader />}
        <ActivityList {...props} />
        {props.repoList.length === 0 && (
          <div className="no-results">{props.user.name} has no Repository</div>
        )}
      </div>
    </div>
  );
};

export default UserActivityView;
