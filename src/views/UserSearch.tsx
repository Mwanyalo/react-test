import { useState } from "react";
import { Preloader } from "../components/Preloader/Preloader";
import UserSearchInput from "../components/UserSearch/UserSearchInput";
import UsersList from "../components/UsersList/UsersList";

const UserSearchView = (props: any) => {
  let [name, setName] = useState("");

  const onSubmitName = (event: any) => {
    event.preventDefault();
    props.searchUsers(name);
  };

  const onInputChange = (event: any) => {
    event.preventDefault();
    setName(event.target.value.toLowerCase());
  };
  console.log("UserSearchView", props.isLoading)

  return (
    <div>
      <UserSearchInput
        name={name}
        onSubmitName={onSubmitName}
        onInputChange={onInputChange}
      />
        {props.isLoading && <Preloader />}
      <UsersList searchList = {props.searchList} />
    </div>
  );
};

export default UserSearchView;
