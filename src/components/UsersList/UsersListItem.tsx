import { Link } from "react-router-dom";

const UsersListItem = (props: any) => {
  return (
    <li key={props.index}>
      <img src={props.user.avatar_url} alt="User avatar" />
      <div>
        {" "}
        <span className="name">{props.user.login}</span>
        <span className="score">Score: {props.user.score}</span>
        <span className="user-type">Profile: {props.user.type}</span>
        <Link className="action" to={`/user-activity/${props.user.login}`}>View User</Link>
      </div>
    </li>
  );
};

export default UsersListItem;
