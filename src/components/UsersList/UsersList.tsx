
import "./UsersList.css";
import UsersListItem from "./UsersListItem";
const UsersList = (props: any) => {
  const { searchList } = props;
  return (
    <div className="container">
      <ul id="user-list">
        {searchList.length > 0 &&
          searchList.map((user: any, index: number) => {
            return <UsersListItem user={user} key={index} />;
          })}
        {searchList.length === 0 && (
          <div className="no-results">No User Found!</div>
        )}
        {searchList.length === undefined && (
          <div className="no-results">Search results will appear here...</div>
        )}
      </ul>
    </div>
  );
};

export default UsersList;
