import "./UserSearchInput.css"
const UserSearchInput = (props: any) => {
  return (
    <div className="search-flex">
      <div className="container">
        <strong>Search Github Users</strong>
        <form onSubmit={props.onSubmitName}>
          <input
            type="text"
            placeholder="Search..."
            value={props.name}
            onChange={props.onInputChange}
            required
          />
          <button
            type="submit"
            className="form-submit"
          >
            Search
          </button>
        </form>
      </div>
    </div>
  );
};

export default UserSearchInput;
