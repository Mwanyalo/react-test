import { FC } from "react";
import img from "../../assets/three-dots.svg";

export const Preloader: FC = () => {
  return (
    <div className="container loading-img">
      <img src={img} alt="Loading..." />
    </div>
  );
};
