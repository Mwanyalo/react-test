import "./UserProfile.css";

const UserProfile = (props: any) => {
  return (

      <div className="profile">
        <div className="profile-sidebar">
          <div className="profile-img">
            <img src={props.avatar_url} alt="" />
          </div>
          <div className="profile-user">
            <div className="profile-name">{props.name}</div>
            <div className="profile-job">{props.company}</div>
          </div>

          <div className="stats">
            <div>
              <p className="number">{props.followers}</p>
              <p className="stat-text">Followers</p>
            </div>
            <div>
              <p className="number"> {props.following}</p>
              <p className="stat-text">Following</p>
            </div>
            <div>
              <p className="number">{props.public_repos}</p>
              <p className="stat-text">Repositories</p>
            </div>
          </div>
      

        <div className="profile-bio">
          <h5>{props.name} Bio</h5>
          <span> {props.bio ? props.bio : "No Bio"}</span>
        </div>
        </div>
      </div>

  );
};

export default UserProfile;
