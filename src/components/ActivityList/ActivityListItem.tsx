import "./ActivityList.css";

const ActivityListItem = (props: any) => {
  return (
    <article className="repo-view">
      <div className="repo-content">
        <div className="repo-language">
          {" "}
          {props.language ? props.language : "No Language specicified"}
        </div>
        <h3 className="repo-title">{props.name}</h3>
        <p className="repo-description">{props.description}</p>

        <div className="stats">
          <div>
            <p className="number">{props.forks}</p>
            <p className="stat-text">Forks</p>
          </div>
          <div>
            <p className="number"> {props.watchers}</p>
            <p className="stat-text">Open Issues</p>
          </div>
          <div>
            <p className="number">{props.open_issues}</p>
            <p className="stat-text">Watchers</p>
          </div>
        </div>
      </div>
    </article>
  );
};

export default ActivityListItem;
