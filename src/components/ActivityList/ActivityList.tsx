import ActivityListItem from "./ActivityListItem";
import "./ActivityList.css";
import { Link } from "react-router-dom";

const ActivityList = (props: any) => {
  const { repoList } = props;
  return (
    <div >
      <div>
        <h4>Repositories ({repoList.length })</h4>
        <Link className="back-nav" to={`/`}>Back to home page</Link>
      </div>
      {repoList.length > 0 &&
        repoList.map((repo: any, index: number) => {
          return <ActivityListItem {...repo} key={index} />;
        })}

    </div>
  );
};

export default ActivityList;
