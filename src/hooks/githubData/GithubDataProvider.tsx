import React, { useState } from 'react';
import GitHubService from '../../services/github/Github';
import { GitHubActivityData, GitHubUserSearchData } from '../../services/github/types';
import { gitHubDataContext, GitHubDataContext } from './GitHubDataContext';

const GitHubDataProvider = ({ children }: { children: React.ReactNode | React.ReactNode[]}) => {
  const githubData = ProvideGitHubData();

  return (
    <gitHubDataContext.Provider value={githubData}>
      { children }
    </gitHubDataContext.Provider>
  )
}

export default GitHubDataProvider;

function ProvideGitHubData(): GitHubDataContext {
  const [searchList, setsearchList] = useState({})
  const [repoList, setRepoList] = useState({})
  const [user, setUser] = useState({})
  const [isLoading, setIsLoading] = useState(false);


  const searchUsers = async (username: string, page?: number) => {
    setsearchList({});
    setIsLoading(true);
    const result = await GitHubService.searchUsers(username, page);
    const {items} = result;
    setsearchList(items)
    setIsLoading(false);
  }

  const getUserRepos = async (username: string) => {
    setRepoList({});
    setIsLoading(true);
    const result = await GitHubService.getUserActivity(username);
    setRepoList(result)
    setIsLoading(false);
  }

  const getUser = async (username: string) => {
    setUser({});
    setIsLoading(true);
    const result = await GitHubService.getUserProfile(username);
    setUser(result)
    setIsLoading(false);
  }

  return {
  searchList,
  repoList,
  searchUsers,
  getUserRepos,
  getUser,
  user,
  isLoading
  }
}