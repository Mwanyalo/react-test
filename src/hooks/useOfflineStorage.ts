interface IUseOfflineStorage {
  saveData: (key: string, data: any) => void;
  loadData: (key: string) => any;
}

export const useOfflineStorage: () => IUseOfflineStorage = () => {
  const saveData = (key: string, data: any) => {
    const storedData: any = localStorage.getItem(key);
    let jsonData = JSON.parse(storedData);
    if (jsonData != null) {
      localStorage.removeItem(key)
    } 
    localStorage.setItem(key, JSON.stringify(data));
  }

  const loadData = (key: string) => {
    let data = localStorage.getItem(key);
    return data;
  }

  return {
    saveData,
    loadData
  }
}