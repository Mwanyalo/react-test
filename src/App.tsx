import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";

import GitHubDataProvider from "./hooks/githubData/GithubDataProvider";
import { gitHubDataContext } from "./hooks/githubData/GitHubDataContext";
import UserActivityView from "./views/UserActivity";
import UserSearchView from "./views/UserSearch";

function App() {
  return (
    <div className="App">
      <GitHubDataProvider>
        <gitHubDataContext.Consumer>
          {(context) => (
            <Router {...context}>
              <Switch>
                <Route path="/" exact>
                  <UserSearchView {...context}/>
                </Route>
                <Route path="/user-activity/:username">
                  <UserActivityView {...context} />
                </Route>
              </Switch>
            </Router>
          )}
        </gitHubDataContext.Consumer>
      </GitHubDataProvider>
    </div>
  );
}

export default App;
